/* ========================================================================== --
                     copyright, Polytech'Tours, 2014-2014

      Ce logiciel est la propri�t� de Polytech'Tours. Il ne peut etre ni 
   communiqu� � des tiers ni utilis� sans autorisation pr�alable �crite de 
                Polytech'Tours. Son contenu ne peut �tre divulgu�.

   This source code contained herein is property of Polytech'Tours and shall 
       not be disclosed or reproduced without the prior authorization of 
                                Polytech'Tours.

   ==========================================================================
   File: gpio.h

   ==========================================================================
   Functional description:
   --------------------------------------------------------------------------
   STM32 GPIO driver data that can be shared by application and basic software.

   ==========================================================================
   History:
   --------------------------------------------------------------------------
   Date: 21.11.2014 - Authors: Name / Name
   + first version

-- ========================================================================== */

#ifndef D_GPIO_H
#define D_GPIO_H

/**-------------------------------------------------------------------------- --
   Include header files
-- -------------------------------------------------------------------------- */
#include "types.h"


/**-------------------------------------------------------------------------- --
   Constants
-- -------------------------------------------------------------------------- */
/** without object */
#define C_GPIO_NB_CHANNEL   64

/**-------------------------------------------------------------------------- --
   Macros
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Types
-- -------------------------------------------------------------------------- */

typedef enum
{
//manuel page 111
//In input mode (MODE[1:0]=00):
Analog_mode=0 //: Analog mode
,Floating_input=1 //: Floating input (reset state)
,Input_with=2 //: Input with pull-up / pull-down
,Reserved=3 //: Reserved
//In output mode (MODE[1:0] > 00):
,push_pull=0 //: General purpose output push-pull
,Open_drain=1 //: General purpose output Open-drain
,Push_pull=2 //: Alternate function output Push-pull
,fct_Open_drain=3 //: Alternate function output Open-drain

} gpio_cnf_t;

typedef enum
{
//manuel page 111
Input_mode=0	//(reset state)
	,Output_mode_10MHz=1   // max speed 10 MHz.
	,Output_mode_2MHz=2	// max speed 2 MHz.
	,Output_mode_50MHz=3   // max speed 50 MHz.
} gpio_mode_t;

typedef struct{ 
    gpio_cnf_t  cnf;
    gpio_mode_t mode;
} gpio_config_t;

/**-------------------------------------------------------------------------- --
   External variables (globals)
-- -------------------------------------------------------------------------- */
/** without object */


/* -------------------------------------------------------------------------- --
   Function prototypes
-- -------------------------------------------------------------------------- */
/** without object */


#endif /* D_GPIO_H */
