/* ========================================================================== --
                     copyright, Polytech'Tours, 2014-2014

      Ce logiciel est la propri�t� de Polytech'Tours. Il ne peut etre ni 
   communiqu� � des tiers ni utilis� sans autorisation pr�alable �crite de 
                Polytech'Tours. Son contenu ne peut �tre divulgu�.

   This source code contained herein is property of Polytech'Tours and shall 
       not be disclosed or reproduced without the prior authorization of 
                                Polytech'Tours.

   ==========================================================================
   File: uart.c

   ==========================================================================
   Functional description:
   --------------------------------------------------------------------------
   Provide functions to manage the STM32 UART device.
   Note : See STM32F100xx reference manual page 576

   ==========================================================================
   History:
   --------------------------------------------------------------------------
   Date: 28.11.2014 - Authors: Name / Name
   + first version

-- ========================================================================== */

/**-------------------------------------------------------------------------- --
   Include header files
-- -------------------------------------------------------------------------- */
#include "drv/uart-priv.h"


/**-------------------------------------------------------------------------- --
   Local constants
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Local macros
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Local types
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   External variables (globals)
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Static variables
-- -------------------------------------------------------------------------- */
uart_dev_t    G_uart[C_UART_NB];

/**-------------------------------------------------------------------------- --
   Private functions prototypes
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Private functions prototypes
-- -------------------------------------------------------------------------- */
/** without object */


/**========================================================================== --
   Private functions
-- ========================================================================== */
/** without object */


/**========================================================================== --
   Public functions
-- ========================================================================== */

/* -------------------------------------------------------------------------- --
   FUNCTION:F_uart_init

   --------------------------------------------------------------------------
   Purpose:
   Initialize STM32 GPIO device
   ...

   --------------------------------------------------------------------------
   Description:
   ...

   --------------------------------------------------------------------------
   Return value:
   ...

-- -------------------------------------------------------------------------- */
int32_t  F_uart_init
(
    uint32_t    i_id            /* device identifier  */
    ,addr_t     i_base_address
    ,uint32_t   clock   /* base address of GPIO registers */
)
{
    
    /* Interrupt processing if device identifier is out of range */
    if ( i_id >= C_UART_NB )
    {
        return -1;
    }

    /* Set base address of uart */
    G_uart[i_id].base_address = (uart_t *)i_base_address;
    
    G_uart[i_id].clock = clock;
    

    /* Return success code */  
    return 0;
}

/**========================================================================== --
   Public functions
-- ========================================================================== */

/* -------------------------------------------------------------------------- --
   FUNCTION:F_uart_setconfig

   --------------------------------------------------------------------------
   Purpose:
   Initialize STM32 GPIO device
   ...

   --------------------------------------------------------------------------
   Description:
   ...

   --------------------------------------------------------------------------
   Return value:
   ...

-- -------------------------------------------------------------------------- */

int32_t  F_uart_setconfig 
(
    uint32_t            i_id            /* device identifier  */
   ,uart_config_t *     i_config        /* configuration data */
)
{
  /* Local variables */
    reg32_t *brr;
    reg32_t *cr1;
    reg32_t *cr2;
 
        /* Interrupt processing if device identifier is out of range */
    if (i_id >= C_UART_NB)
    {
        return -1;
    }
//get register value 
brr = &G_uart[i_id].base_address->USART_BRR; 
cr1 = &G_uart[i_id].base_address->USART_CR1;
cr2 = &G_uart[i_id].base_address->USART_CR2;

// update the value of cr1 for re and te configuration 
M_bupdate32((*cr1),UART_CR1_RE , 1);

M_bupdate32((*cr1),UART_CR1_TE , 1);

// configuration of the parity bits 
if (i_config->parity == 0 ){
    
    M_bupdate32((*cr1),UART_CR1_PCE , 0);

}
else if (i_config->parity == 1){
    
    M_bupdate32((*cr1),UART_CR1_PCE , 1);
    M_bupdate32((*cr1),UART_CR1_PS , 0);

}
else if (i_config->parity == 2){
    
    M_bupdate32((*cr1),UART_CR1_PCE , 1);
    M_bupdate32((*cr1),UART_CR1_PS , 1);

}

//configuration of datawidth (for us 8N1)
M_bupdate32((*cr1),UART_CR1_M , i_config->datawidth);
// configuration of enabling (if 0 nothing can append)
M_bupdate32((*cr1),UART_CR1_UE , 1);
// configuration of the stop bit with the value 00 01 11 10 (check on the uart_priv.h)
M_bupdate32((*cr2),UART_CR2_STOP,i_config->nbstop);


//config of the clock rate 
M_bupdate32((*brr),UART_BSRR_DIV,G_uart[i_id].clock/i_config->rate);

    return 0;
}
/**========================================================================== --
   Public functions
-- ========================================================================== */

/* -------------------------------------------------------------------------- --
   FUNCTION:F_uart_putc

   --------------------------------------------------------------------------
   Purpose:
   Initialize STM32 GPIO device
   ...

   --------------------------------------------------------------------------
   Description:
   ...

   --------------------------------------------------------------------------
   Return value:
   ...

-- -------------------------------------------------------------------------- */

int32_t F_uart_putc // function to put a character 
(
    uint32_t            i_id            /* device identifier  */
   ,uint32_t            value       /* configuration data */
)
{
    //while the buffer is full nothing can append 
while (M_bget(G_uart[i_id].base_address->USART_SR, UART_SR_TXE)==0){
}
//else fulling the buffer with the caracter (value )
G_uart[i_id].base_address->USART_DR=value; 

return 0;
}

/**========================================================================== --
   Public functions
-- ========================================================================== */

/* -------------------------------------------------------------------------- --
   FUNCTION:F_uart_getc

   --------------------------------------------------------------------------
   Purpose:
   Initialize STM32 GPIO device
   ...

   --------------------------------------------------------------------------
   Description:
   ...

   --------------------------------------------------------------------------
   Return value:
   ...

-- -------------------------------------------------------------------------- */

int32_t F_uart_getc
(
    uint32_t            i_id            /* device identifier  */  
)
{
    //while the buffer is empty nothing can append 
while (M_bget(G_uart[i_id].base_address->USART_SR, UART_SR_RXNE)==0){
}
//If the buffer is full return the value stuck in the buffer 
return G_uart[i_id].base_address->USART_DR;
}
/**========================================================================== --
   Public functions
-- ========================================================================== */

/* -------------------------------------------------------------------------- --
   FUNCTION:F_uart_write

   --------------------------------------------------------------------------
   Purpose:
   Initialize STM32 GPIO device
   ...

   --------------------------------------------------------------------------
   Description:
   ...

   --------------------------------------------------------------------------
   Return value:
   ...

-- -------------------------------------------------------------------------- */

int32_t F_uart_write
(
    uint32_t     id, 
    char        wdata[], 
    uint32_t    wsize            /* device identifier  */  
)
{
    
int i ;
for(i=0;i<wsize;i++){
//for the size of the word put all of the caracter in the buffer 
F_uart_putc(0,wdata[i]);

} 


return 0;
}

/**========================================================================== --
   Public functions
-- ========================================================================== */

/* -------------------------------------------------------------------------- --
   FUNCTION:F_uart_read

   --------------------------------------------------------------------------
   Purpose:
   Initialize STM32 GPIO device
   ...

   --------------------------------------------------------------------------
   Description:
   ...

   --------------------------------------------------------------------------
   Return value:
   ...

-- -------------------------------------------------------------------------- */

int32_t F_uart_read
(
       uint32_t          id, 
       char             rdata[], 
       uint32_t          rsize            /* device identifier  */  
)
{

    
int i ;
for(i=0;i<rsize;i++){

rdata[i]=F_uart_getc(0);
//for the size of the word get all of the caracter in the buffer step by step 
} 


return 0;
}
