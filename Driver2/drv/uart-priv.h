/* ========================================================================== --
                     copyright, Polytech'Tours, 2014-2014

      Ce logiciel est la propri�t� de Polytech'Tours. Il ne peut etre ni 
   communiqu� � des tiers ni utilis� sans autorisation pr�alable �crite de 
                Polytech'Tours. Son contenu ne peut �tre divulgu�.

   This source code contained herein is property of Polytech'Tours and shall 
       not be disclosed or reproduced without the prior authorization of 
                                Polytech'Tours.

   ==========================================================================
   File: uart-priv.h

   ==========================================================================
   Functional description:
   --------------------------------------------------------------------------
   Private data and prototype of STM32 UART driver.

   ==========================================================================
   History:
   --------------------------------------------------------------------------
   Date: 28.11.2014 - Authors: Name / Name
   + first version

-- ========================================================================== */

#ifndef D_UART_PRIV_H
#define D_UART_PRIV_H

/**-------------------------------------------------------------------------- --
   Include header files
-- -------------------------------------------------------------------------- */
#include "uart.h"


/**-------------------------------------------------------------------------- --
   Constants
-- -------------------------------------------------------------------------- */
#define UART_CR1_UE     13, 13      /* USART enable ok
                                       When this bit is cleared the USART prescalers and outputs are stopped and the end of the
                                       current
                                       byte transfer in order to reduce power consumption. This bit is set and cleared by software.
                                       0: USART prescaler and outputs disabled
                                       1: USART enabled */

#define UART_CR1_M     12,12
                                     /* Word length ok 
                                        This bit determines the word length. It is set or cleared by software.
                                        0: 1 Start bit, 8 Data bits, n Stop bit
                                        1: 1 Start bit, 9 Data bits, n Stop bit
                                        Note: The M bit must not be modified during a data transfer (both transmission and reception)*/

#define UART_CR1_WAKE    11,11
                                     /* Wakeup method
                                        This bit determines the USART wakeup method, it is set or cleared by software.
                                        0: Idle Line
                                        1: Address Mark */
#define UART_CR1_PCE    10,10   
                                     /* Parity control enable ok 
                                        This bit selects the hardware parity control (generation and detection). When the parity
                                        control is enabled, the computed parity is inserted at the MSB position (9th bit if M=1; 8th bit
                                        if M=0) and parity is checked on the received data. This bit is set and cleared by software.
                                        Once it is set, PCE is active after the current byte (in reception and in transmission).
                                        0: Parity control disabled
                                        1: Parity control enabled */

#define UART_CR1_PS     9,9
                                     /* Parity selection ok 
                                        This bit selects the odd or even parity when the parity generation/detection is enabled (PCE
                                        bit set). It is set and cleared by software. The parity will be selected after the current byte.
                                        0: Even parity
                                        1: Odd parity */
#define UART_CR1_PEIE     8,8
                                     /* PE interrupt enable
                                        This bit is set and cleared by software.
                                        0: Interrupt is inhibited
                                        1: A USART interrupt is generated whenever PE=1 in the USART_SR register */
#define UART_CR1_TXEIE    7,7
                                     /* TXE interrupt enable
                                        This bit is set and cleared by software.
                                        0: Interrupt is inhibited
                                        1: A USART interrupt is generated whenever TXE=1 in the USART_SR register */
#define UART_CR1_TCIE   6,6
                                     /* Transmission complete interrupt enable
                                        This bit is set and cleared by software.
                                        0: Interrupt is inhibited
                                        1: A USART interrupt is generated whenever TC=1 in the USART_SR register */
#define UART_CR1_RXNEIE   5,5
                                     /* RXNE interrupt enable
                                        This bit is set and cleared by software.
                                        0: Interrupt is inhibited
                                        1: A USART interrupt is generated whenever ORE=1 or RXNE=1 in the USART_SR register*/
#define UART_CR1_IDLEIE 4,4
                                     /* IDLE interrupt enable
                                        This bit is set and cleared by software.
                                        0: Interrupt is inhibited
                                        1: A USART interrupt is generated whenever IDLE=1 in the USART_SR register*/
#define UART_CR1_TE     3,3
                                     /* Transmitter enable
                                        This bit enables the transmitter. It is set and cleared by software.
                                        0: Transmitter is disabled
                                        1: Transmitter is enabled
                                        Note: 1: During transmission, a �0� pulse on the TE bit (�0� followed by �1�) sends a preamble
                                        (idle line) after the current word, except in smartcard mode.
                                        2: When TE is set there is a 1 bit-time delay before the transmission starts.*/
#define UART_CR1_RE     2,2
                                     /* Receiver enable ok 
                                        This bit enables the receiver. It is set and cleared by software.
                                        0: Receiver is disabled
                                        1: Receiver is enabled and begins searching for a start bit*/
#define UART_CR1_RWU    1,1
                                     /* Receiver wakeup
                                        This bit determines if the USART is in mute mode or not. It is set and cleared by software and
                                        can be cleared by hardware when a wakeup sequence is recognized.
                                        0: Receiver in active mode
                                        1: Receiver in mute mode
                                        Note: 1: Before selecting Mute mode (by setting the RWU bit) the USART must first receive a
                                        data byte, otherwise it cannot function in Mute mode with wakeup by Idle line detection.
                                        2: In Address Mark Detection wakeup configuration (WAKE bit=1) the RWU bit cannot
                                        be modified by software while the RXNE bit is set.*/
#define UART_CR1_SBK    0,0
                                     /* Send break 
                                        This bit set is used to send break characters. It can be set and cleared by software. It should
                                        be set by software, and will be reset by hardware during the stop bit of break.
                                        0: No break character is transmitted
                                        1: Break character will be transmitted */


#define UART_CR2_LINEN   14,14
                                        /*Bit 14 LINEN: LIN mode enable
                                        This bit is set and cleared by software.
                                        0: LIN mode disabled
                                        1: LIN mode enabled
                                        The LIN mode enables the capability to send LIN Synch Breaks (13 low bits) using the SBK
                                        bit in the USART_CR1 register, and to detect LIN Sync breaks.*/
#define UART_CR2_STOP   13,12
                                        /*Bits 13:12 STOP: STOP bits
                                        These bits are used for programming the stop bits.
                                        00: 1 Stop bit
                                        01: 0.5 Stop bit
                                        10: 2 Stop bits
                                        11: 1.5 Stop bit
                                        The 0.5 Stop bit and 1.5 Stop bit are not available for UART4 & UART5.*/
#define UART_CR2_CLKEN  11,11
                                        /*Bit 11 CLKEN: Clock enable
                                        This bit allows the user to enable the CK pin.
                                        0: CK pin disabled
                                        1: CK pin enabled
                                        This bit is not available for UART4 & UART5.*/
#define UART_CR2_CPOL  10,10                                       
                                        /*Bit 10 CPOL: Clock polarity
                                        This bit allows the user to select the polarity of the clock output on the CK pin in synchronous
                                        mode. It works in conjunction with the CPHA bit to produce the desired clock/data
                                        relationship
                                        0: Steady low value on CK pin outside transmission window.
                                        1: Steady high value on CK pin outside transmission window.
                                        This bit is not available for UART4 & UART5.*/
#define UART_CR2_CPHA  9,9
                                        /*Bit 9 CPHA: Clock phase
                                        This bit allows the user to select the phase of the clock output on the CK pin in synchronous
                                        mode. It works in conjunction with the CPOL bit to produce the desired clock/data
                                        relationship (see figures 254 to 255)
                                        0: The first clock transition is the first data capture edge.
                                        1: The second clock transition is the first data capture edge.
                                        This bit is not available for UART4 & UART5   */                                    
 #define UART_CR2_LBCL  8,8                                       
                                        /*Bit 8 LBCL: Last bit clock pulse
                                        This bit allows the user to select whether the clock pulse associated with the last data bit
                                        transmitted (MSB) has to be output on the CK pin in synchronous mode.
                                        0: The clock pulse of the last data bit is not output to the CK pin
                                        1: The clock pulse of the last data bit is output to the CK pin
                                        The last bit is the 8th or 9th data bit transmitted depending on the 8 or 9 bit format selected
                                        by the M bit in the USART_CR1 register.
                                        This bit is not available for UART4 & UART5.*/
#define UART_CR2_LBDIE   6,6
                                        /*Bit 6 LBDIE: LIN break detection interrupt enable
                                        Break interrupt mask (break detection using break delimiter).
                                        0: Interrupt is inhibited
                                        1: An interrupt is generated whenever LBD=1 in the USART_SR register*/
#define UART_CR2_LBDL   5,5
                                        /*Bit 5 LBDL: lin break detection length
                                        This bit is for selection between 11 bit or 10 bit break detection.
                                        0: 10 bit break detection
                                        1: 11 bit break detection*/
#define UART_CR2_ADD    3,0
                                        /*Bits 3:0 ADD[3:0]: Address of the USART node
                                        This bit-field gives the address of the USART node.
                                        This is used in multiprocessor communication during mute mode, for wake up with address
                                        mark detection.*/


#define UART_CR3_CTSIE    10,10
                                        /*Bit 10 CTSIE: CTS interrupt enable
                                        0: Interrupt is inhibited
                                        1: An interrupt is generated whenever CTS=1 in the USART_SR register
                                        This bit is not available for UART4 & UART5.*/
 #define UART_CR3_CTSE  9,9                                     
                                        //Bit 9 CTSE: CTS enable
                                        //0: CTS hardware flow control disabled
                                        //1: CTS mode enabled, data is only transmitted when the nCTS input is asserted (tied to 0). If
                                        //the nCTS input is deasserted while a data is being transmitted, then the transmission is
                                        //completed before stopping. If a data is written into the data register while nCTS is asserted,
                                        //the transmission is postponed until nCTS is asserted.
                                        //This bit is not available for UART4 & UART5.
#define UART_CR3_RTSE   8,8
                                        //Bit 8 RTSE: RTS enable
                                        //0: RTS hardware flow control disabled
                                        //1: RTS interrupt enabled, data is only requested when there is space in the receive buffer.
                                        //The transmission of data is expected to cease after the current character has been
                                        //transmitted. The nRTS output is asserted (tied to 0) when a data can be received.
                                        //This bit is not available for UART4 & UART5.
#define UART_CR3_DMAT   7,7
                                        //Bit 7 DMAT: DMA enable transmitter
                                        //This bit is set/reset by software
                                        //1: DMA mode is enabled for transmission
                                        //0: DMA mode is disabled for transmission
                                        //This bit is not available for UART5.
#define UART_CR3_DMAR   6,6
                                        //Bit 6 DMAR: DMA enable receiver
                                        //This bit is set/reset by software
                                        //1: DMA mode is enabled for reception
                                        //0: DMA mode is disabled for reception
                                        //This bit is not available for UART5.
#define UART_CR3_SCEN   5,5
                                        //Bit 5 SCEN: Smartcard mode enable
                                        //This bit is used for enabling Smartcard mode.
                                        //0: Smartcard Mode disabled
                                        //1: Smartcard Mode enabled
                                        //This bit is not available for UART4 & UART5.
#define UART_CR3_NACK   4,4
                                        //Bit 4 NACK: Smartcard NACK enable
                                        //0: NACK transmission in case of parity error is disabled
                                        //1: NACK transmission during parity error is enabled
                                        //This bit is not available for UART4 & UART5.
#define UART_CR3_HDSEL  3,3
                                        //Bit 3 HDSEL: Half-duplex selection
                                        //Selection of Single-wire Half-duplex mode
                                        //0: Half duplex mode is not selected
                                        //1: Half duplex mode is selected
#define UART_CR3_IRLP   2,2
                                        //Bit 2 IRLP: IrDA low-power
                                        //This bit is used for selecting between normal and low-power IrDA modes
                                        //0: Normal mode
                                        //1: Low-power mode
#define UART_CR3_IREN   1,1
                                        //Bit 1 IREN: IrDA mode enable
                                        //This bit is set and cleared by software.
                                        //0: IrDA disabled
                                        //1: IrDA enabled
#define UART_CR3_EIE    0,0
                                        //Bit 0 EIE: Error interrupt enable
                                        //Error Interrupt Enable Bit is required to enable interrupt generation in case of a framing error,
                                        //overrun error or noise error (FE=1 or ORE=1 or NE=1 in the USART_SR register) in case of
                                        //Multi Buffer Communication (DMAR=1 in the USART_CR3 register).
                                        //0: Interrupt is inhibited
                                        //1: An interrupt is generated whenever DMAR=1 in the USART_CR3 register and FE=1 or
                                        //ORE=1 or NE=1 in the USART_SR register

#define UART_BSRR_DIV_MANTISSA 15,4                                
                                     /* DIV_Mantissa[11:0]: mantissa of USARTDIV
                                        These 12 bits define the mantissa of the USART Divider (USARTDIV)*/
#define UART_BSRR_DIV_FRACTION 3,0                                  
                                     /* DIV_Fraction[3:0]: fraction of USARTDIV
                                        These 4 bits define the fraction of the USART Divider (USARTDIV)*/
#define UART_BSRR_DIV 15,0 



#define UART_DR_DATA_VALUE    8,0
                                        /*Bits 8:0 DR[8:0]: Data value
                                        Contains the Received or Transmitted data character, depending on whether it is read from
                                        or written to.
                                        The Data register performs a double function (read and write) since it is composed of two
                                        registers, one for transmission (TDR) and one for reception (RDR)
                                        The TDR register provides the parallel interface between the internal bus and the output
                                        shift register (see Figure 1).
                                        The RDR register provides the parallel interface between the input shift register and the
                                        internal bus.
                                        When transmitting with the parity enabled (PCE bit set to 1 in the USART_CR1 register), the
                                        value written in the MSB (bit 7 or bit 8 depending on the data length) has no effect because
                                        it is replaced by the parity.
                                        When receiving with the parity enabled, the value read in the MSB bit is the received parity
                                        bit.*/
#define UART_SR_CTS      9,9
                                        //Bit 9 CTS: CTS flag
                                        //This bit is set by hardware when the nCTS input toggles, if the CTSE bit is set. It is cleared
                                        //by software (by writing it to 0). An interrupt is generated if CTSIE=1 in the USART_CR3
                                        //register.
                                        //0: No change occurred on the nCTS status line
                                        //1: A change occurred on the nCTS status line
                                        //This bit is not available for UART4 & UART5.
#define UART_SR_LBD      8,8
                                        //Bit 8 LBD: LIN break detection flag
                                        //This bit is set by hardware when the LIN break is detected. It is cleared by software (by
                                        //writing it to 0). An interrupt is generated if LBDIE = 1 in the USART_CR2 register.
                                        //0: LIN Break not detected
                                        //1: LIN break detected
                                        //Note: An interrupt is generated when LBD=1 if LBDIE=1
#define UART_SR_TXE     7,7
                                        //Bit 7 TXE: Transmit data register empty
                                        //This bit is set by hardware when the content of the TDR register has been transferred into
                                        //the shift register. An interrupt is generated if the TXEIE bit =1 in the USART_CR1 register. It
                                        //is cleared by a write to the USART_DR register.
                                        //0: Data is not transferred to the shift register
                                        //1: Data is transferred to the shift register)
                                        //Note: This bit is used during single buffer transmission.
#define UART_SR_TC      6,6
                                        //Bit 6 TC: Transmission complete
                                        //This bit is set by hardware if the transmission of a frame containing data is complete and if
                                        //TXE is set. An interrupt is generated if TCIE=1 in the USART_CR1 register. It is cleared by a
                                        //software sequence (a read from the USART_SR register followed by a write to the
                                        //USART_DR register). The TC bit can also be cleared by writing a '0' to it. This clearing
                                        //sequence is recommended only for multibuffer communication.
                                        //0: Transmission is not complete
                                        //1: Transmission is complete
#define UART_SR_RXNE    5,5
                                        //Bit 5 RXNE: Read data register not empty
                                        //This bit is set by hardware when the content of the RDR shift register has been transferred to
                                        //the USART_DR register. An interrupt is generated if RXNEIE=1 in the USART_CR1 register.
                                        //It is cleared by a read to the USART_DR register. The RXNE flag can also be cleared by
                                        //writing a zero to it. This clearing sequence is recommended only for multibuffer
                                        //communication.
                                        //0: Data is not received
                                        //1: Received data is ready to be read.
#define UART_SR_IDLE     4,4
                                        //Bit 4 IDLE: IDLE line detected
                                        //This bit is set by hardware when an Idle Line is detected. An interrupt is generated if the
                                        //IDLEIE=1 in the USART_CR1 register. It is cleared by a software sequence (an read to the
                                        //USART_SR register followed by a read to the USART_DR register).
                                        //0: No Idle Line is detected
                                        //1: Idle Line is detected
                                        //Note: The IDLE bit will not be set again until the RXNE bit has been set itself (i.e. a new idle
                                        //line occurs).
#define UART_SR_ORE     3,3
                                        //Bit 3 ORE: Overrun error
                                        //This bit is set by hardware when the word currently being received in the shift register is
                                        //ready to be transferred into the RDR register while RXNE=1. An interrupt is generated if
                                        //RXNEIE=1 in the USART_CR1 register. It is cleared by a software sequence (an read to the
                                        //USART_SR register followed by a read to the USART_DR register).
                                        //0: No Overrun error
                                        //1: Overrun error is detected
                                        //Note: When this bit is set, the RDR register content will not be lost but the shift register will be
                                        //overwritten. An interrupt is generated on ORE flag in case of Multi Buffer
                                        //communication if the EIE bit is set.
#define UART_SR_NE      2,2
                                        //Bit 2 NE: Noise error flag
                                        //This bit is set by hardware when noise is detected on a received frame. It is cleared by a
                                        //software sequence (an read to the USART_SR register followed by a read to the
                                        //USART_DR register).
                                        //0: No noise is detected
                                        //1: Noise is detected
                                        //Note: This bit does not generate interrupt as it appears at the same time as the RXNE bit
                                        //which itself generates an interrupting interrupt is generated on NE flag in case of Multi
                                        //Buffer communication if the EIE bit is set.
#define UART_SR_FE      1,1
                                        //Bit 1 FE: Framing error
                                        //This bit is set by hardware when a de-synchronization, excessive noise or a break character
                                        //is detected. It is cleared by a software sequence (an read to the USART_SR register
                                        //followed by a read to the USART_DR register).
                                        //0: No Framing error is detected
                                        //1: Framing error or break character is detected
                                        //Note: This bit does not generate interrupt as it appears at the same time as the RXNE bit
                                        //which itself generates an interrupt. If the word currently being transferred causes both
                                        //frame error and overrun error, it will be transferred and only the ORE bit will be set.
                                        //An interrupt is generated on FE flag in case of Multi Buffer communication if the EIE bit
                                        //is set.
#define UART_SR_PE      0,0
                                        //Bit 0 PE: Parity error
                                        //This bit is set by hardware when a parity error occurs in receiver mode. It is cleared by a
                                        //software sequence (a read to the status register followed by a read to the USART_DR data
                                        //register). The software must wait for the RXNE flag to be set before clearing the PE bit.
                                        //An interrupt is generated if PEIE = 1 in the USART_CR1 register.
                                        //0: No parity error
                                        //1: Parity error

#define UART_GTPR_GT    15,8

                                        /*Bits 15:8 GT[7:0]: Guard time value
                                        This bit-field gives the Guard time value in terms of number of baud clocks.
                                        This is used in Smartcard mode. The Transmission Complete flag is set after this guard time
                                        value.
                                        This bit is not available for UART4 & UART5.*/
#define UART_GTPR_PSC   7,0
                                        /*Bits 7:0 PSC[7:0]: Prescaler value
                                        � In IrDA Low-power mode:
                                        PSC[7:0] = IrDA Low-Power Baud Rate
                                        Used for programming the prescaler for dividing the system clock to achieve the low-power
                                        frequency:
                                        The source clock is divided by the value given in the register (8 significant bits):
                                        00000000: Reserved - do not program this value
                                        00000001: divides the source clock by 1
                                        00000010: divides the source clock by 2
                                        ...
                                        � In normal IrDA mode: PSC must be set to 00000001.
                                        � In smartcard mode:
                                        PSC[4:0]: Prescaler value
                                        Used for programming the prescaler for dividing the system clock to provide the smartcard
                                        clock.
                                        The value given in the register (5 significant bits) is multiplied by 2 to give the division factor
                                        of the source clock frequency:
                                        00000: Reserved - do not program this value
                                        00001: divides the source clock by 2
                                        00010: divides the source clock by 4
                                        00011: divides the source clock by 6
                                        ...
                                        Note: Bits [7:5] have no effect if Smartcard mode is used.
                                        This bit is not available for UART4 & UART5.*/
/**-------------------------------------------------------------------------- --
   Macros
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Types
-- -------------------------------------------------------------------------- */
/** see reference manual page 576 and 675 */

typedef struct
{
reg32_t USART_SR;
reg32_t USART_DR;
reg32_t USART_BRR;
reg32_t USART_CR1;
reg32_t USART_CR2;
reg32_t USART_CR3;
reg32_t USART_GTPR;


} uart_t;   // set uart T structure     

    /** ------------------------------------------------------------------ */

typedef struct
{
  uart_t * base_address;
  uint32_t clock;

} uart_dev_t;   /* UART device data */

/**-------------------------------------------------------------------------- --
   External variables (globals)
-- -------------------------------------------------------------------------- */
/** without object */


/* -------------------------------------------------------------------------- --
   Function prototypes
-- -------------------------------------------------------------------------- */
int32_t  F_uart_init        ( uint32_t i_id, addr_t base_address,uint32_t clock); // declaration of  init function
int32_t  F_uart_setconfig   ( uint32_t i_id, uart_config_t * i_config ); //declaration of setconfig function 
int32_t  F_uart_putc        ( uint32_t i_id, uint32_t value ); //declaration of putc function 
int32_t  F_uart_getc        ( uint32_t i_id ); //declaration of getc function 
int32_t  F_uart_write       ( uint32_t id ,char wdata[],uint32_t wsize );  // declaration of write function
int32_t  F_uart_read        ( uint32_t id,char  rdata[],uint32_t rsize );  // declaration of read function 

#endif /* D_UART_PRIV_H */
