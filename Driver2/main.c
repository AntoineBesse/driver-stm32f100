/* ========================================================================== --
                     copyright, Polytech'Tours, 2013-2013

      Ce logiciel est la propri�t� de Polytech'Tours. Il ne peut etre ni 
   communiqu� � des tiers ni utilis� sans autorisation pr�alable �crite de 
                Polytech'Tours. Son contenu ne peut �tre divulgu�.

   This source code contained herein is property of Polytech'Tours and shall 
       not be disclosed or reproduced without the prior authorization of 
                                Polytech'Tours.

   ==========================================================================
   File: main.c

   ==========================================================================
   Functional description:
   --------------------------------------------------------------------------
   Main file of UART and GPIO drivers

   ==========================================================================
   History:
   --------------------------------------------------------------------------
   Date: 22.11.2013 - Authors: Name / Name
   + first version

-- ========================================================================== */

/**-------------------------------------------------------------------------- --
   Include header files
-- -------------------------------------------------------------------------- */
#include "types.h"
#include "drv/uart-priv.h"
#include "drv/rcc-priv.h"
#include "drv/afio-priv.h"
#include "types.h"
#include "drv/gpio.h"
#include "drv/gpio-priv.h"
/**-------------------------------------------------------------------------- --
   Local constants
-- -------------------------------------------------------------------------- */
/** without object */
#define GPIO_A_ID  0
#define GPIO_A_ADR  0x40010800

/**-------------------------------------------------------------------------- --
   Local macros
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Local types
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   External variables (globals)
-- -------------------------------------------------------------------------- */
rcc_dev_t	G_rcc;
afio_dev_t  G_afio;

/**-------------------------------------------------------------------------- --
   Static variables
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Private functions prototypes
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Private functions prototypes
-- -------------------------------------------------------------------------- */
/** without object */


/**========================================================================== --
   Private functions
-- ========================================================================== */

/* -------------------------------------------------------------------------- --
   FUNCTION:
   F_test_uart

   --------------------------------------------------------------------------
   Purpose:
   Test UART driver

   --------------------------------------------------------------------------
   Description:
   This function checks UART driver.

   --------------------------------------------------------------------------
   Return value:
     0, in nominal case
     -1, otherwise

-- -------------------------------------------------------------------------- */
int  F_test_uart  ( void )
{
    char chaine[] = "Salut";
    int size = 5;
    
    uart_config_t config; // variable for uart config 
    gpio_config_t configb;// variable for gpio config 
    F_uart_init(0,0x40013800,8000000);
     
// initialisation of the UART structure configuration 
config.datawidth= 0;
config.nbstop = 00;
config.parity= EVEN;
config.rate= 115200;



// initialisation of the GPIO structure configuration 
configb.mode=Output_mode_50MHz;
configb.cnf=push_pull;
F_gpio_init( GPIO_A_ID, GPIO_A_ADR );
F_gpio_setconfig( GPIO_A_ID, 9, &configb );

//set configuration of the UART 
F_uart_setconfig(0,&config);

//write the choosing word 
F_uart_write(0,chaine,size);

//read the choosing word 
F_uart_read(0,chaine,size);  
   
    return 0;
}



/**========================================================================== --
   Public functions
-- ========================================================================== */


/* -------------------------------------------------------------------------- --
   FUNCTION:
   main

   --------------------------------------------------------------------------
   Purpose:
   Main function

   --------------------------------------------------------------------------
   Description:
   This function is the entry point of the software.

   --------------------------------------------------------------------------
   Return value:
     0

-- -------------------------------------------------------------------------- */
 int  main  ( void )
 {
    /* Local variables */
    /** without object */

    G_rcc.base_address  = (rcc_t *)0x40021000;
    G_afio.base_address = (afio_t *)0x40010000;
    
    /* Enable clock for Alternate function I/O */
    G_rcc.base_address->APB2ENR |= 0x01;        /* RCC_APB2ENR_AFIOEN */

    /* Disable USART1 remap */
    /* It controls the mapping of USART1 TX and RX alternate functions on the GPIO ports */
    /* USART1.TX = PA.9, USART1.RX = PA.10 */
    G_afio.base_address->MAPR   &= ~(1 << 2);                 
    
    /* Enable clock for USART1 */
    G_rcc.base_address->APB2ENR |= 0x01 << 14; /* RCC_APB2ENR_USART1EN; */

    /* Check UART */
    F_test_uart();
    
    return 0;
}
