/* ========================================================================== --
                     copyright, Polytech'Tours, 2014-2014

      Ce logiciel est la propri�t� de Polytech'Tours. Il ne peut etre ni 
   communiqu� � des tiers ni utilis� sans autorisation pr�alable �crite de 
                Polytech'Tours. Son contenu ne peut �tre divulgu�.

   This source code contained herein is property of Polytech'Tours and shall 
       not be disclosed or reproduced without the prior authorization of 
                                Polytech'Tours.

   ==========================================================================
   File: gpio.c

   ==========================================================================
   Functional description:
   --------------------------------------------------------------------------
   Provide functions to manage the STM32 GPIO device.
     Note : See STM32F100xx reference manual page 99

   ==========================================================================
   History:
   --------------------------------------------------------------------------
   Date: 21.11.2014 - Authors: Name / Name
   + first version

-- ========================================================================== */

/**-------------------------------------------------------------------------- --
   Include header files
-- -------------------------------------------------------------------------- */
#include "drv/gpio-priv.h"


/**-------------------------------------------------------------------------- --
   Local constants
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Local macros
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Local types
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   External variables (globals)
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Static variables
-- -------------------------------------------------------------------------- */
gpio_dev_t    G_gpio[C_GPIO_NB];


/**-------------------------------------------------------------------------- --
   Private functions prototypes
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Private functions prototypes
-- -------------------------------------------------------------------------- */
/** without object */


/**========================================================================== --
   Private functions
-- ========================================================================== */
/** without object */


/**========================================================================== --
   Public functions
-- ========================================================================== */

/* -------------------------------------------------------------------------- --
   FUNCTION:
   F_gpio_init

   --------------------------------------------------------------------------
   Purpose:
   Initialize STM32 GPIO device

   --------------------------------------------------------------------------
   Description:
   This function initializes the GPIO 'i_id' in function of parameters.
     
   --------------------------------------------------------------------------
   Return value:
   0, in nominal case
     ...

-- -------------------------------------------------------------------------- */
int32_t  F_gpio_init
(
    uint32_t    i_id            /* device identifier  */
   ,addr_t      i_base_address  /* base address of GPIO registers */
)
{
    
    /* Interrupt processing if device identifier is out of range */
    if ( i_id >= C_GPIO_NB )
    {
        return -1;
    }

    /* Set base address of gpio */
    G_gpio[i_id].base_address = (gpio_t *)i_base_address;

    /* Return success code */    
    return 0;
}


/* -------------------------------------------------------------------------- --
   FUNCTION:
   F_gpio_setconfig

   --------------------------------------------------------------------------
   Purpose:
   Configure STM32 GPIO device

   --------------------------------------------------------------------------
   Description:
   This function configures the channel 'i_channel_id' of GPIO 'i_id' in 
     function of 'i_config' data.
     
   --------------------------------------------------------------------------
   Return value:
   0, in nominal case
     ...

-- -------------------------------------------------------------------------- */
int32_t  F_gpio_setconfig //manuel P110 confgpio modegpio 
(
    uint32_t            i_id            /* device identifier  */
   ,uint32_t            i_channel_id    /* channel identifier */
   ,gpio_config_t *     i_config        /* configuration data */
)
{
    /* Local variables */
    reg32_t *crx;
        /* Interrupt processing if device identifier is out of range */
    if (i_id >= C_GPIO_NB)
    {
        return -1;
    }

    /* Interrupt processing if channel identifier is out of range */
    if ( i_channel_id >= C_GPIO_NB_CHANNEL )
    {
        return -2;
    }
    
        /* Get GPIO configuration register if function of 'i_channel_id' */
    if ( i_channel_id < 8 )
    {
        crx = &G_gpio[i_id].base_address->GPIO_CRL;
        /* Return the state of the IO 'i_channel_id' */
    (*crx) = M_field_reg( (*crx)
                        , (i_channel_id*4)+1   //msb
                        , i_channel_id*4   //lsb
                        , i_config->mode
                        );
                        
   (*crx) = M_field_reg( (*crx)
                        , (i_channel_id*4)+3   //msb
                        , (i_channel_id*4)+2   //lsb
                        , i_config->cnf
                        ); 
   
    }
    else
    {
        crx = &G_gpio[i_id].base_address->GPIO_CRH;
                /* Return the state of the IO 'i_channel_id' */
    (*crx) = M_field_reg( (*crx)
                        , ((i_channel_id-8)*4)+1   //msb
                        , (i_channel_id-8)*4   //lsb
                        , i_config->mode
                        );
                        
    (*crx) = M_field_reg( (*crx)
                        , ((i_channel_id-8)*4)+3   //msb
                        , ((i_channel_id-8)*4)+2   //lsb
                        , i_config->cnf
                        ); 
 
    }

    /* Return success code */
    return 0;
}


/* -------------------------------------------------------------------------- --
   FUNCTION:
   F_gpio_write

   --------------------------------------------------------------------------
   Purpose:
   Set discrete state

   --------------------------------------------------------------------------
   Description:
   This function sets the output 'i_channel' of GPIO 'i_id' to 'i_state'.
     
   --------------------------------------------------------------------------
   Return value:
   R�sultat de l'ecriture

-- -------------------------------------------------------------------------- */
int32_t F_gpio_write
(
    uint32_t    i_id                /* device identifier  */
   ,uint32_t    i_channel_id        /* channel identifier */
   ,uint32_t    i_state             /* value to set on output channel */
)
{
    /* Local variables */
    reg32_t *data;
    
       /* Interrupt processing if device identifier is out of range */
    if (i_id >= C_GPIO_NB)
    {
        return -1;
    }

    /* Interrupt processing if channel identifier is out of range */
    if ( i_channel_id >= C_GPIO_NB_CHANNEL )
    {
        return -2;
    }
    
    /* Get GPIO data register if function of 'i_channel_id' */
 if ( i_channel_id < 16 )
    {
        data = &G_gpio[i_id].base_address->GPIO_ODR;
    }
    else
    {
        return -3; // error register reserved 
    }
 
    
  /* Return the state of the IO 'i_channel_id' */
    (*data) = M_field_reg( (*data)
                        , i_channel_id   //msb
                        , i_channel_id   //lsb
                        , i_state
                        );
                        
    /* Return success code */
    return 0;
}


/* -------------------------------------------------------------------------- --
   FUNCTION:
   F_gpio_read

   --------------------------------------------------------------------------
   Purpose:
   Get discrete state

   --------------------------------------------------------------------------
   Description:
   This function returns the state of channel 'i_channel_id' of GPIO 'i_id'.

   --------------------------------------------------------------------------
   Return value:
     0 or 1, discrete state
   -1, if failed

-- -------------------------------------------------------------------------- */
int32_t  F_gpio_read
(
    uint32_t    i_id            /* device identifier  */
   ,uint32_t    i_channel_id    /* channel identifier */
)
{
       /* Local variables */
    reg32_t *data;
    
       /* Interrupt processing if device identifier is out of range */
    if (i_id >= C_GPIO_NB)
    {
        return -1;
    }

    /* Interrupt processing if channel identifier is out of range */
    if ( i_channel_id >= C_GPIO_NB_CHANNEL )
    {
        return -2;
    }
        
    /* Get GPIO data register if function of 'i_channel_id' */
if ( i_channel_id < 16 )
    {
        data = &G_gpio[i_id].base_address->GPIO_IDR;
    }
    else
    {
        return -3; // error register reserved 
    }


    /* Return the state of the IO 'i_channel_id' */
    return M_field_get( (*data)
                      , i_channel_id   // msb
                      , i_channel_id   // lsb
                      );
  
}
