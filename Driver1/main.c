/* ========================================================================== --
                     copyright, Polytech'Tours, 2016-2016

      Ce logiciel est la propri�t� de Polytech'Tours. Il ne peut etre ni 
   communiqu� � des tiers ni utilis� sans autorisation pr�alable �crite de 
                Polytech'Tours. Son contenu ne peut �tre divulgu�.

   This source code contained herein is property of Polytech'Tours and shall 
       not be disclosed or reproduced without the prior authorization of 
                                Polytech'Tours.

   ==========================================================================
   File: main.c

   ==========================================================================
   Functional description:
   --------------------------------------------------------------------------
   ...

   ==========================================================================
   History:
   --------------------------------------------------------------------------
   Date: xx.11.2016 - Authors: First and last name
   + first version

-- ========================================================================== */

/**-------------------------------------------------------------------------- --
   Include header files
-- -------------------------------------------------------------------------- */
#include "types.h"
#include "drv/gpio.h"
#include "drv/gpio-priv.h"



/**-------------------------------------------------------------------------- --
   Local constants
-- -------------------------------------------------------------------------- */
#define GPIO_A_ID  0  // constant for gpio A id 
#define GPIO_B_ID  1  // constant for gpio B id 
#define GPIO_A_ADR  0x40010800 // constant for gpio A add (see on the datasheet p36) 
#define GPIO_B_ADR  0x40010C00 // constant for gpio B add (see on the datasheet p36)


/**-------------------------------------------------------------------------- --
   Local macros
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Local types
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   External variables (globals)
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Static variables
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Private functions prototypes
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Private functions prototypes
-- -------------------------------------------------------------------------- */
/** without object */


/**========================================================================== --
   Private functions
-- ========================================================================== */

/* -------------------------------------------------------------------------- --
   FUNCTION:
   F_test_gpio

   --------------------------------------------------------------------------
   Purpose:
   Test GPIO driver

   --------------------------------------------------------------------------
   Description:
   This function checks GPIO driver.

   --------------------------------------------------------------------------
   Return value:
     0, in nominal case
     -1, otherwise

-- -------------------------------------------------------------------------- */
int  F_test_gpio  ( void )
{
    int ini =0;
    int oldvalue =1;
    int newvalue =1;
    int oldvalue13 =1;
    int newvalue13 =1;
    int count =0 ;
    int temp=0;
    gpio_config_t config;        
//-----------------------------------------------------------------------------------    
/*init*/

F_gpio_init( GPIO_A_ID, GPIO_A_ADR ); 
F_gpio_init( GPIO_B_ID, GPIO_B_ADR );

//-----------------------------------------------------------------------------------    
/*Setconfig*/
config.mode=Input_mode; 
config.cnf=Input_with;

F_gpio_setconfig( GPIO_A_ID, 0, &config );//set config of PA.0
F_gpio_setconfig( GPIO_A_ID, 13, &config ); //set config of PA.0


config.mode=Output_mode_50MHz;
config.cnf=push_pull;

for (ini = 0 ; ini < 16 ; ini++)
{
  F_gpio_setconfig( GPIO_B_ID, ini, &config ); //set config of PB.0 to PB.16
  F_gpio_write(GPIO_B_ID, ini,0);  
}
  F_gpio_write(GPIO_B_ID, 0,1);

//-----------------------------------------------------------------------------------    
/*Detect Button*/

while(1){  

/*Configuration of reading GPIO 0*/ 
 
    newvalue= F_gpio_read(GPIO_A_ID,13); //read the value PA.13 (button)
    
  if (oldvalue!=newvalue&&oldvalue==1){ // focus on the value N and N-1
    
    F_gpio_write(GPIO_B_ID, temp,0); //read the value PA.0 (button)  
    count++;
//maximum scope of the pin number gestion
    if(count>15){ 
        count=0;
        temp=15;

    } 
   F_gpio_write(GPIO_B_ID, count,1);
temp=count;
  }
    oldvalue = newvalue ;
    
/*Configuration of reading GPIO 13*/ 
// we search the diff�rence between old value and new value if oldvalue ==1 and newvalue == 0 ==> detection of down scale 
    newvalue13=F_gpio_read(GPIO_A_ID,0); 
 if (oldvalue13!=newvalue13&&oldvalue13==1){


F_gpio_write(GPIO_B_ID, temp,0);   // Delete Precedent count   
count--;
//min scope of the pin number gestion  
if (count<0){
    count=15;
    temp=0;    
    
  
 }
   F_gpio_write(GPIO_B_ID,count,1);
   temp=count;

  }
    oldvalue13 = newvalue13 ;
    
}
    return 0; // not reachable 
}



/**========================================================================== --
   Public functions
-- ========================================================================== */


/* -------------------------------------------------------------------------- --
   FUNCTION:
   main

   --------------------------------------------------------------------------
   Purpose:
   Main function

   --------------------------------------------------------------------------
   Description:
   This function is the entry point of the software.

   --------------------------------------------------------------------------
   Return value:
     0

-- -------------------------------------------------------------------------- */
 int  main  ( void )
 
 
 {
     /*used fonction */
 

F_test_gpio  (); // use the function test gpio 
    
   return 0; 
}
