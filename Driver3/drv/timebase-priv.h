/* ========================================================================== --
                     copyright, Polytech'Tours, 2014-2014

      Ce logiciel est la propri�t� de Polytech'Tours. Il ne peut etre ni 
   communiqu� � des tiers ni utilis� sans autorisation pr�alable �crite de 
                Polytech'Tours. Son contenu ne peut �tre divulgu�.

   This source code contained herein is property of Polytech'Tours and shall 
       not be disclosed or reproduced without the prior authorization of 
                                Polytech'Tours.

   ==========================================================================
   File: timebase-priv.h

   ==========================================================================
   Functional description:
   --------------------------------------------------------------------------
   Timebase data and prototypes that shall be accessed only by basic software.

   ==========================================================================
   History:
   --------------------------------------------------------------------------
   Date: 10.12.2014 - Authors: Name / Name
   + first version

-- ========================================================================== */

#ifndef D_TIMEBASE_PRIV_H
#define D_TIMEBASE_PRIV_H

/**-------------------------------------------------------------------------- --
   Include header files
-- -------------------------------------------------------------------------- */
#include "types.h"


/**-------------------------------------------------------------------------- --
   Constants
-- -------------------------------------------------------------------------- */

/**-------------------------------------------------------------------------- --
   Macros
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Types
-- -------------------------------------------------------------------------- */

typedef struct
{
    reg32_t  RTC_CRH;       /* control register high */
    reg32_t  RTC_CRL;       /* control register low */
    reg32_t  RTC_PRLH;      /* prescaler load register high */
    reg32_t  RTC_PRLL;      /* prescaler load register low */
    reg32_t  RTC_DIVH;      /* prescaler divider register high */
    reg32_t  RTC_DIVL;      /* prescaler divider register low */
    reg32_t  RTC_CNTH;      /* counter register high */
    reg32_t  RTC_CNTL;      /* counter register low */
    reg32_t  RTC_ALRH;      /* alarm register high */
    reg32_t  RTC_ALRL;      /* alarm register low */
    
} rtc_t; //register structure   

typedef struct
{
	rtc_t * base_address;
    uint32_t	 mult;
	uint32_t	 div;

} timebase_dev_t;   /* timebase device data */



/**-------------------------------------------------------------------------- --
   External variables (globals)
-- -------------------------------------------------------------------------- */
/** without object */


/* -------------------------------------------------------------------------- --
   Function prototypes
-- -------------------------------------------------------------------------- */
uint32_t    F_timebase_init          (addr_t      i_base_address); // function init 
void        F_timebase_setconfig     ( addr_t, uint32_t, uint32_t ); // function setconfig 
uint32_t    F_timebase_get           ( uint64_t * ); //function get 
void        F_wait                   ( uint32_t ); // wait (use timer)


#endif /* D_TIMEBASE_PRIV_H */
