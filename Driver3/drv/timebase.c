/* ========================================================================== --
                     copyright, Polytech'Tours, 2014-2014

      Ce logiciel est la propri�t� de Polytech'Tours. Il ne peut etre ni 
   communiqu� � des tiers ni utilis� sans autorisation pr�alable �crite de 
                Polytech'Tours. Son contenu ne peut �tre divulgu�.

   This source code contained herein is property of Polytech'Tours and shall 
       not be disclosed or reproduced without the prior authorization of 
                                Polytech'Tours.

   ==========================================================================
   File: timebase.c

   ==========================================================================
   Functional description:
   --------------------------------------------------------------------------
   Provide functions to manage the timebase of STM32.
   Note : See STM32F100xx reference manual page 452 (RTC device)

   ==========================================================================
   History:
   --------------------------------------------------------------------------
   Date: 10.12.2014 - Authors: Name / Name
   + first version

-- ========================================================================== */

/**-------------------------------------------------------------------------- --
   Include header files
-- -------------------------------------------------------------------------- */
#include "drv/timebase-priv.h"


/**-------------------------------------------------------------------------- --
   Local constants
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Local macros
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Local types
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   External variables (globals)
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Static variables
-- -------------------------------------------------------------------------- */
timebase_dev_t	G_timebase; // second global declaration 


/**-------------------------------------------------------------------------- --
   Private functions prototypes
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Private functions prototypes
-- -------------------------------------------------------------------------- */
/** without object */


/**========================================================================== --
   Private functions
-- ========================================================================== */
/** without object */

/**========================================================================== --
   Public functions
-- ========================================================================== */

/* -------------------------------------------------------------------------- --
   FUNCTION:F_timebase_init

   --------------------------------------------------------------------------
   Purpose:
  

   --------------------------------------------------------------------------
   Description:

-- -------------------------------------------------------------------------- */
uint32_t F_timebase_init
(
    addr_t      i_base_address  /* base address of timebase device */
     
)
{

    /* Set base address of device */
    G_timebase.base_address = (rtc_t *)i_base_address; // init adresse 

    /* Return success code */    
    return 0  ; 
}
/**========================================================================== --
   Public functions
-- ========================================================================== */

/* -------------------------------------------------------------------------- --
   FUNCTION:F_timebase_setconfig

   --------------------------------------------------------------------------
   Purpose:
  

   --------------------------------------------------------------------------
   Description:

-- -------------------------------------------------------------------------- */
void  F_timebase_setconfig
(
    addr_t      i_base_address  /* base address of timebase device */
   ,uint32_t    i_mult    	    /* multiplier factor of timebase conversion */
   ,uint32_t    i_div           /* divisor factor of timebase conversion */
)
{
    /* Local variables */
    rtc_t *    rtc;
    
    G_timebase.base_address = (rtc_t *)i_base_address;
    G_timebase.mult = i_mult;
    G_timebase.div  = i_div;

//get base adress 
    rtc = G_timebase.base_address;

// attribut value "base_adresse" on the register 
    rtc->RTC_CRL |= 0x0010;
    rtc->RTC_PRLL = 0x0000;
    rtc->RTC_PRLH = 0x0000;
    rtc->RTC_CRL &= 0xFFEF;
    
   
}


/* -------------------------------------------------------------------------- --
   FUNCTION:
   F_timebase_get

   --------------------------------------------------------------------------
   Purpose:
   Read timebase of processor.

   --------------------------------------------------------------------------
   Description:
   Return the elapsed time since initialization of timebase and set 'io_Time' 
   with full scale of timebase (eg. 64bits) if not define to C_NULL.

   --------------------------------------------------------------------------
   Return value:
   * LSB timebase value in �s

-- -------------------------------------------------------------------------- */
uint32_t  F_timebase_get
(
    uint64_t * ms_time	/*  time value in �s */
)
{
    /* Local variables */
    rtc_t * rtc;
    uint16_t	 cnth;		/* upper timebase value */
    uint16_t	cnth2;		/* upper timebase value (second read) */
    uint16_t	 cnt;		/* lower timebase value */
    uint32_t     time;

//get base adresse in variable rtc 
    rtc = G_timebase.base_address;
//get  register 
    cnth  = rtc->RTC_CNTH;
    cnt  = rtc->RTC_CNTL;
    cnth2 = rtc->RTC_CNTH;
// check the type of the register 
    if ( cnth != cnth2 )
    {
        cnt = rtc->RTC_CNTL;
    }
// take the times in twin register and step cnth2 to 4 bits 
    time = (((uint32_t)cnth2) << 16) | (uint32_t)cnt;

    if ( ms_time != C_NULL )// check if time is null
    {

        (*ms_time) = (((uint64_t)time) * G_timebase.mult) / G_timebase.div;
        //get time in the variable ms_time 
    }

//return time 
    return (time * G_timebase.mult) / G_timebase.div;
}


/* -------------------------------------------------------------------------- --
   FUNCTION:
   F_wait

   --------------------------------------------------------------------------
   Purpose:
   Wait a defined time.
 
   --------------------------------------------------------------------------
   Description:
  

-- -------------------------------------------------------------------------- */
void  F_wait
(
    uint32_t  i_delay        /*in : waiting duration in us */
)
{
    /** Local variables */
    uint32_t  ref_time;       
    uint32_t  delta_time;   


   //get time  
    ref_time = F_timebase_get( C_NULL );
    delta_time = 0;     
    /* Repeat */
    while( delta_time< i_delay )
    {
        //Wait during the choosing delay 
        delta_time = F_timebase_get(C_NULL) - ref_time;
    }
    
    

}
