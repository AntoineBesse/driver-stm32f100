/* ========================================================================== --
                     copyright, Polytech'Tours, 2013-2013

      Ce logiciel est la propri�t� de Polytech'Tours. Il ne peut etre ni 
   communiqu� � des tiers ni utilis� sans autorisation pr�alable �crite de 
                Polytech'Tours. Son contenu ne peut �tre divulgu�.

   This source code contained herein is property of Polytech'Tours and shall 
       not be disclosed or reproduced without the prior authorization of 
                                Polytech'Tours.

   ==========================================================================
   File: main.c

   ==========================================================================
   Functional description:
   --------------------------------------------------------------------------
   Main file of UART and GPIO drivers

   ==========================================================================
   History:
   --------------------------------------------------------------------------
   Date: 22.11.2013 - Authors: Name / Name
   + first version

-- ========================================================================== */

/**-------------------------------------------------------------------------- --
   Include header files
-- -------------------------------------------------------------------------- */
#include "types.h"
#include "drv/timebase-priv.h"
#include "drv/rcc-priv.h"
#include "drv/gpio.h"
#include "drv/gpio-priv.h"
#include "drv/gpio.c"
/**-------------------------------------------------------------------------- --
   Local constants
-- -------------------------------------------------------------------------- */
/** without object */
#define GPIO_B_ID  1
#define GPIO_B_ADR  0x40010C00

/**-------------------------------------------------------------------------- --
   Local macros
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Local types
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   External variables (globals)
-- -------------------------------------------------------------------------- */
// set global rcc 
rcc_dev_t	G_rcc;

/**-------------------------------------------------------------------------- --
   Static variables
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Private functions prototypes
-- -------------------------------------------------------------------------- */
/** without object */


/**-------------------------------------------------------------------------- --
   Private functions prototypes
-- -------------------------------------------------------------------------- */
/** without object */


/**========================================================================== --
   Private functions
-- ========================================================================== */

/* -------------------------------------------------------------------------- --
   FUNCTION:
   F_test_timebase

   --------------------------------------------------------------------------
   Purpose:
   Test UART driver

   --------------------------------------------------------------------------
   Description:
   This function checks timebase driver.

   --------------------------------------------------------------------------
   Return value:
     0, in nominal case
     -1, otherwise

-- -------------------------------------------------------------------------- */
int  F_test_timebase  ( void )
{
    /* Local variables */
    uint32_t start;
    uint32_t delta_time;
    int32_t  i;
    int mod ;  
    gpio_config_t configt;   

// init gpio  PB.0 and set config 
configt.mode=Output_mode_50MHz;
configt.cnf=push_pull;
F_gpio_init( GPIO_B_ID, GPIO_B_ADR );
F_gpio_setconfig( GPIO_B_ID, 0, &configt );    

//Init time base 
     F_timebase_init( 0x40002800);
    
    /* Set config timebase (lsb = 16 �s : 8 MHz / 128) */
    F_timebase_setconfig( 0x40002800, 16, 1 );

 
    for ( i = 0; i < 10; i ++ ){
//get start time 
    start = F_timebase_get( C_NULL );
    
 // mod value is 0 or 1 for toggle PB.0   
 mod = i%2;
// write gpio base on the value of mod  
F_gpio_write(GPIO_B_ID,0,mod); 

// wait 1second 
    F_wait(1000000);

//Get delta time for verify the Ftime value 
   delta_time=F_timebase_get( C_NULL ) - start;
    
    }


    /* Return success code */
    return 0;
}



/**========================================================================== --
   Public functions
-- ========================================================================== */


/* -------------------------------------------------------------------------- --
   FUNCTION:
   main

   --------------------------------------------------------------------------
   Purpose:
   Main function

   --------------------------------------------------------------------------
   Description:
   This function is the entry point of the software.

   --------------------------------------------------------------------------
   Return value:
     0

-- -------------------------------------------------------------------------- */
 int  main  ( void )
 {
    /* Local variables */
    /** without object */

    G_rcc.base_address  = (rcc_t *)0x40021000;
    
    /* Power & Backup clock enable, set PWREN & BKPEN bits */
    G_rcc.base_address->APB1ENR |= 0x18000000;             
    
    /* Enable write access to Backup domain, set DBP bit in PWR register*/
    *(reg32_t *)0x40007000 = 0x0100;
    
    /* Reset Backup domain to change source */
    G_rcc.base_address->BDCR |= 0x00010000;
    G_rcc.base_address->BDCR &= 0xFFFEFFFF;
    
    /* Select the RTC Clock */
    G_rcc.base_address->BDCR |= 0x00000300;
    
    /* Set RTCEN bit */
    G_rcc.base_address->BDCR |= 0x00008000;  

    /* Check TIMEBASE */
    F_test_timebase();
}
